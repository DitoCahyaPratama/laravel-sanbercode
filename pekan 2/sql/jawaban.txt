1.  CREATE DATABASE myshop;
setelah membuat database lakukan perintah berikut "USE myshop" untuk memindah database ke database myshop

2. CREATE TABLE users(
    -> id INTEGER AUTO_INCREMENT PRIMARY KEY,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255));

desc users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
	
   CREATE TABLE categories(
    -> id INTEGER AUTO_INCREMENT PRIMARY KEY,
    -> name varchar(255));

desc categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+

   CREATE TABLE items(
    -> id INTEGER AUTO_INCREMENT PRIMARY KEY,
    -> name varchar(255),
    -> description varchar(255),
    -> price INTEGER,
    -> stock INTEGER,
    -> category_id INTEGER,
    -> FOREIGN KEY category_id_idxfk (category_id) REFERENCES categories (id));

desc items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
    
3. INSERT INTO users
    -> (name, email, password)
    -> VALUES
    -> ('John Doe', 'john@doe.com', 'john123'),
    -> ('Jane Doe', 'jane@doe.com', 'jenita123');


   INSERT INTO categories
    -> (name)
    -> VALUES
    -> ('gadget'),
    -> ('cloth'),
    -> ('men'),
    -> ('women'),
    -> ('branded');

  
   INSERT INTO items
    -> (name, description, price, stock, category_id)
    -> VALUES
    -> ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
    -> ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
    -> ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);


4.
 
A. SELECT id, name, email FROM users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+

B. SELECT * FROM items WHERE price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+

   SELECT * FROM items WHERE name LIKE '%uniklo%';
+----+----------+-------------------------------+--------+-------+-------------+
| id | name     | description                   | price  | stock | category_id |
+----+----------+-------------------------------+--------+-------+-------------+
|  2 | Uniklooh | baju keren dari brand ternama | 500000 |    50 |           2 |
+----+----------+-------------------------------+--------+-------+-------------+

C. SELECT items.*, categories.name FROM items
    -> JOIN
    -> categories
    -> ON categories.id = items.category_id;
+----+-------------+-----------------------------------+---------+-------+-------------+--------+
| id | name        | description                       | price   | stock | category_id | name   |
+----+-------------+-----------------------------------+---------+-------+-------------+--------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+----+-------------+-----------------------------------+---------+-------+-------------+--------+

5. UPDATE items SET price = 2500000 WHERE id = 1;

   SELECT * FROM items;

+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+