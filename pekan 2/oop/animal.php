<?php
    class Animal{
        protected $legs = 2;
        protected $cold_blooded = false;
        protected $nama;
        public function __construct($nama)
        {
            $this->name = $nama;
        }
        public function get_name(){
            return $this->name;
        }

        public function get_legs(){
            return $this->legs;
        }

        public function get_cold_blooded(){
            if($this->cold_blooded == true){
                return "true";
            }else{
                return "false";
            }
        }

    }
?>