<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    include 'animal.php';
    include 'Ape.php';
    include 'Frog.php';

    $sheep = new Animal("shaun");

    echo $sheep->get_name(); // "shaun"
    echo $sheep->get_legs(); // 2
    echo $sheep->get_cold_blooded(); // false

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    // index.php
    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"
    echo $sungokong->get_legs();

    $kodok = new Frog("buduk");
    $kodok->jump(); // "hop hop"
    ?>
</body>

</html>