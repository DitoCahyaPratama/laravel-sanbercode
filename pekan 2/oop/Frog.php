<?php
    class Frog extends Animal{
        protected $legs = 4;
        public function __construct($name)
        {
            $this->name = $name;
        }
        public function jump(){
            echo "hop hop";
        }
    }
?>