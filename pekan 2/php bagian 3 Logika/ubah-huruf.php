<?php
function ubah_huruf($string){
//kode di sini
    $abjad = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
    $data = str_split($string);
    $jadi = '';
    for ($i=0; $i < sizeof($data); $i++) {
        for ($j=0; $j < sizeof($abjad); $j++) { 
            if($data[$i] == $abjad[$j]){
                    $jadi = $jadi . $abjad[$j+1];
                    break;
            }
        }
    }
    return $jadi. "<br/>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
