<?php
function skor_terbesar($arr)
{
  //kode di sini
  $kelas = array("Laravel" => array("nilai" => 0),
                "React Native" => array("nilai" => 0),
                "React JS" => array("nilai" => 0));
  for ($i = 0; $i < sizeof($arr); $i++) {
    switch ($arr[$i]['kelas']) {
      case 'Laravel':
        if($arr[$i]['nilai'] > $kelas['Laravel']['nilai']){
          $kelas['Laravel'] = $arr[$i];
        }
        break;
      case 'React Native':
        if($arr[$i]['nilai'] > $kelas['React Native']['nilai']){
          $kelas['React Native'] = $arr[$i];
        }
        break;
      case 'React JS':
        if($arr[$i]['nilai'] > $kelas['React JS']['nilai']){
          $kelas['React JS'] = $arr[$i];
        }
        break;
      default:
        break;
    }
  }
  return $kelas;
}

// TEST CASES
$skor = [
  [
    "nama" => "Bobby",
    "kelas" => "Laravel",
    "nilai" => 78
  ],
  [
    "nama" => "Regi",
    "kelas" => "React Native",
    "nilai" => 86
  ],
  [
    "nama" => "Aghnat",
    "kelas" => "Laravel",
    "nilai" => 90
  ],
  [
    "nama" => "Indra",
    "kelas" => "React JS",
    "nilai" => 85
  ],
  [
    "nama" => "Yoga",
    "kelas" => "React Native",
    "nilai" => 77
  ],
];

print_r(skor_terbesar($skor));
/* OUTPUT
  Array (
    [Laravel] => Array
              (
                [nama] => Aghnat
                [kelas] => Laravel
                [nilai] => 90
              )
    [React Native] => Array
                  (
                    [nama] => Regi
                    [kelas] => React Native
                    [nilai] => 86
                  )
    [React JS] => Array
                (
                  [nama] => Indra
                  [kelas] => React JS
                  [nilai] => 85
                )
  )
*/
