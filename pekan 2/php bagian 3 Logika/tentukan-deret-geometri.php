<?php
function tentukan_deret_geometri($arr)
{
    // kode di sini
    $condition = "false";
    $first = 0;
    $second = 0;
    for ($i = 0; $i < sizeof($arr); $i++) {
        if ($i == 0 || $i == 1 || $i == sizeof($arr) - 1) {
            if($i == 0){
                $first = $arr[0];
            }else if($i == 1){
                $second = $arr[1] / $arr[0];
            }
        } else if (($arr[$i] * $second) == $arr[$i + 1]) {
            $condition = "true";
        } else {
            $condition = "false";
        }
    }
    return $condition . "<br/>";
}
//TEST CASES
echo tentukan_deret_geometri([1, 3, 9, 27, 81]); // true
echo tentukan_deret_geometri([2, 4, 8, 16, 32]); // true
echo tentukan_deret_geometri([2, 4, 6, 8]); // false
echo tentukan_deret_geometri([2, 6, 18, 54]); // true
echo tentukan_deret_geometri([1, 2, 3, 4, 7, 9]); //false
