<?php
function pasangan_terbesar($angka)
{
    // kode di sini
    $data = str_split($angka);
    // print_r($data);
    $temp = 0;
    $couple = '';
    for ($i = 0; $i < strlen($angka); $i++) {
        if($i == strlen($angka) - 1){
            for ($j = $i; $j < $i + 1; $j++) {
                $couple = $couple . $data[$j];
            }
        }else{
            for ($j = $i; $j < $i + 2; $j++) {
                $couple = $couple . $data[$j];
            }
        }
        // echo $couple . " ";
        if ($couple > $temp) {
            $temp = $couple;
            $couple = '';
        }else{
            $couple = '';
        }
    }
    echo "<br />";
    return $temp;
}

// TEST CASES
echo pasangan_terbesar(641573); // 73
echo pasangan_terbesar(12783456); // 83
echo pasangan_terbesar(910233); // 91
echo pasangan_terbesar(71856421); // 85
echo pasangan_terbesar(79918293); // 99