<?php

// algoritma
/*
    memanggil function hitung
    deklarasi newdata mengubah string ke array
    deklarasi hasil
    melakukan perulangan sebanyak count dari newData
    jika value dari index berupa angka maka tidak melakukan apa apa
    jika value selain angka angka maka lakukan pengisian data untuk bil 1 , bil 2 dan operator
    untuk pengisian bil1 dimulai dari index 0 hingga index sebelum operator
    untuk operator adalah index selain angka-angka sebelumnya
    untuk pengisian bil2 dimulai dari index setelah operator hingga sebanyak jumlah dari newdata
    mengubah bil1 dan bil2 ke int dengan melakukan implode
    dilanjutkan pemilihan operator dan pengisian hasil
    mengembalikan nilai hasil
    selesai
*/

function hitung($string_data)
{
    $newData = str_split($string_data);
    $hasil = 0;
    for ($i = 0; $i < count($newData); $i++) {
        if ($newData[$i] == '0' || $newData[$i] == '1' || $newData[$i] == '2' || $newData[$i] == '3' || $newData[$i] == '4' || $newData[$i] == '5' || $newData[$i] == '6' || $newData[$i] == '7' || $newData[$i] == '8' || $newData[$i] == '9') {
        } else {
            for ($j = 0; $j < $i; $j++) {
                $bil1[] = $newData[$j];
            }
            $operator = $newData[$i];
            for ($j = $i + 1; $j < sizeof($newData); $j++) {
                $bil2[] = $newData[$j];
            }
        }
    }
    $first = implode($bil1);
    $second = implode($bil2);
    switch ($operator) {
        case '+':
            $hasil = $first + $second;
            break;
        case '-':
            $hasil = $first - $second;
            break;
        case '*':
            $hasil = $first * $second;
            break;
        case ':':
            $hasil = $first / $second;
            break;
        case '%':
            $hasil = $first % $second;
            break;
    }
    return $hasil."\n";
}

// TEST CASES
echo hitung("102*2"); //204
echo hitung("2+3");  //5
echo hitung("100:25"); //4
echo hitung("10%2"); //0
echo hitung("99-2"); //97
?>