<?php

// algoritma
/*
    memanggil function perolehan_medali
    deklarasi negara yang mana terdiri dari indonesia, india dan korea selatan
    kemudian dilakukan looping sebanyak jumlah negara yang dideklarasikan
    dalam looping dilakukan deklarasi emas, perak dan perunggu
    dilakukan nested looping sebanyak jumlah array untuk mengisi masing-masing negara
    jika nama negara dari array sama dengan nama dari variabel negara maka
    dilakukan permilihan untuk index ke 1 dari array
    jika emas maka emas++, jika perak maka perak++, jika perunggu maka perunggu++
    selanjutnya dilakukan pengecekan untuk newarray yang mana isinya adalah array assosiative yang berisi nama negara dan jumlah medali
    jika perulangan dari variabel array selesai maka akan dilakukan penambahan data pada variabel finisharr
    dan yang terakhir adalah mereturn data dari variabel finisharr
    selesai
*/

    function perolehan_medali($array){
        $negara = array('Indonesia', 'India', 'Korea Selatan');

        for ($a=0; $a < count($negara); $a++) { 
            $emas = 0;
            $perak = 0;
            $perunggu = 0;
            for ($i=0; $i < count($array); $i++) {     
                if($array[$i][0] == $negara[$a]){
                    switch($array[$i][1]){
                        case 'emas':
                            $emas++;
                            break;
                        case 'perak':
                            $perak++;
                            break;
                        case 'perunggu':
                            $perunggu++;
                            break;
                    }
                    $newArr = array("negara" => $negara[$a], "emas" => $emas, "perak" => $perak, "perunggu" => $perunggu);
                }
            }    
            $finishArr[] = $newArr;  
        }
        return $finishArr;
    }
    print_r(perolehan_medali(
        array(
            array('Indonesia', 'emas'),
            array('India', 'perak'),
            array('Korea Selatan', 'emas'),
            array('India', 'perak'),
            array('India', 'emas'),
            array('Indonesia', 'perak'),
            array('Indonesia', 'emas'),
        )
    ))
?>